# Path ------------------------------------------------------------

# NOTE! I think here shouldn't be too much stuff.. only source 
# .bashrc. PATH-stuff could be put before the .bashrc's 
# interactively-check..

export PATH=/opt/local/bin:/opt/local/sbin:/usr/local/mysql/bin:$PATH  # OS-X Specific, with MacPorts and MySQL installed

# Added to make android sdk work
export ANDROID_HOME=/Users/ouzoinio/Development/adt-bundle-mac-x86_64/sdk
export PATH=$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH
# End of android sdk things

if [ -d ~/bin ]; then
	export PATH=:~/bin:$PATH  # add your bin folder to the path, if you have it.  It's a good place to add all your scripts
fi


# Load in .bashrc -------------------------------------------------
if [ -f ~/.bashrc ]; then
source ~/.bashrc
fi

# Load in .bash_aliases -------------------------------------------
if [ -f ~/.bash_aliases ]; then
source ~/.bash_aliases
fi



# Hello Messsage --------------------------------------------------
echo -e "Kernel Information: " `uname -smr`
echo -e "${COLOR_BROWN}`bash --version`"
echo -ne "${COLOR_GRAY}Uptime: "; uptime
echo -ne "${COLOR_GRAY}Server time is: "; date


# Notes: ----------------------------------------------------------
# When you start an interactive shell (log in, open terminal or iTerm in OS X, 
# or create a new tab in iTerm) the following files are read and run, in this order:
#     profile
#     bashrc
#     .bash_profile
#     .bashrc (only because this file is run (sourced) in .bash_profile)
#
# When an interactive shell, that is not a login shell, is started 
# (when you run "bash" from inside a shell, or when you start a shell in 
# xwindows [xterm/gnome-terminal/etc] ) the following files are read and executed, 
# in this order:
#     bashrc
#     .bashrc
eval "$(rbenv init - --norehash)"
