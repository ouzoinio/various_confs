"Vimrc by Juuso Soinio
"
"

" Hides buffers and doesnt force to write your
" changes on them
set hidden

" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %

" We are running vim, not vi
set nocompatible

" Set the sound off
set visualbell

" Change leader key to ,
:let mapleader = ","

"Better copy and paste
set pastetoggle=<C-p>
set clipboard=unnamed

" Easier moving of code blocks
vnoremap < <gv 
vnoremap > >gv

"Enable syntax highlighting
filetype plugin indent on
syntax on

" Showing line numbers and length
set number  " Show line number
set tw=79   " width of the document
set nowrap  " don't automatically wrap on load
set ff-=t   " don't automatically wrap text when typing
set colorcolumn=80
highlight ColorColumn ctermbg=233

" Easier formatting of paragraphs
vmap Q gq
nmap Q gqap

" Useful settings
set history=100
set undolevels=100

" Tab settings
set tabstop=2
set softtabstop=2
set shiftwidth=2
set shiftround
set expandtab

" Make search case insensitive
set hlsearch
set incsearch
set ignorecase
set smartcase

" Disable backup and swap files. Too many filesystem events
set nobackup
set nowritebackup
set noswapfile

" Set search to highlight all the occurences of word
set hlsearch

" Vundle configuration
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" ##################################################
" Plugins. Maintained by Vundle
" ##################################################
" Required!
Bundle 'gmarik/vundle'

" Github repos:
"
Bundle 'millermedeiros/vim-statline.git'
Bundle 'wincent/Command-T.git'
Bundle 'MarcWeber/vim-addon-mw-utils.git'
Bundle 'altercation/vim-colors-solarized.git'
Bundle 'kchmck/vim-coffee-script.git'
Bundle 'tpope/vim-fugitive.git'
Bundle 'scrooloose/syntastic.git'
Bundle 'tmhedberg/matchit.git'
Bundle 'majutsushi/tagbar'

" Snipmate and dependencies
Bundle "MarcWeber/vim-addon-mw-utils"
Bundle "tomtom/tlib_vim"
Bundle "honza/snipmate-snippets"
Bundle "garbas/vim-snipmate"

" vim-scripts repos
" example: Bundle 'FuzzyFinder'

" Non github repos
" example: Bundle 'git://git.example.com/keijo'

" ##################################################
"  END of plugins 
" ##################################################

" Enable solarized colorscheme
syntax enable
set background=dark
colorscheme solarized
