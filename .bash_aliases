# Separate file for aliases used in bash

# Enable colors if possible.. ------------------------------------------------------------------

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# General use aliases --------------------------------------------------------------------------
alias ll='ls -hl'
alias la='ls -a'
alias lla='ls -lah'
alias ls='ls -G'  # OS-X SPECIFIC - the -G command in OS-X is for colors, in Linux it's no groups
#alias ls='ls --color=auto' # For linux, etc
alias ..='cd ..'
alias ...='cd .. ; cd ..'
# Lists all the colors, uses vars in .bashrc_non-interactive
alias colorslist="set | egrep 'COLOR_\w*'"

# Misc
alias g='grep -i'  # Case insensitive grep
alias f='find . -iname'
alias ducks='du -cksh * | sort -rn|head -11' # Lists folders and files sizes in the current folder
alias top='top -o cpu'
alias systail='tail -f /var/log/system.log'
alias m='more'
alias df='df -h'

# Shows most used commands, cool script I got this from: http://lifehacker.com/software/how-to/turbocharge-your-terminal-274317.php
alias profileme="history | awk '{print \$2}' | awk 'BEGIN{FS=\"|\"}{print \$1}' | sort | uniq -c | sort -n | tail -n 20 | sort -nr"

#netcat rulezzz
alias kissa='nc'

alias sqlite='sqlite3'

#start working faster:
function work {
	cd ~/projects/restaurantapp_backend/

	osascript 2>/dev/null <<EOF
	tell application "System Events"
	 	tell process "Terminal" to keystroke "t" using command down
	end
	tell application "Terminal"
	  activate
	  do script with command "cd ~/projects/restaurantapp_backend/" in window 1
	  do script with command "guard" in window 1
	end tell
EOF

osascript -e "tell application \"Terminal\"" \
    -e "tell application \"System Events\" to keystroke \"ö\" using {command down, shift down}" \
		-e "do script with command \"mate ~/projects/restaurantapp_backend\" in window 1" \
    -e "end tell"
    > /dev/null

} 		



# Local Apache error logging
alias apachelog="tail -f /private/var/log/apache2/error_log"


# You need to create fmdiff and fmresolve, which can be found at: http://ssel.vub.ac.be/ssel/internal:fmdiff
alias svdiff='sv diff --diff-cmd fmdiff' # OS-X SPECIFIC
# Use diff for command line diff, use fmdiff for gui diff, and svdiff for subversion diff


alias sv='svn --username ${SV_USER}'
alias svimport='sv import'
alias svcheckout='sv checkout'
alias svstatus='sv status'
alias svupdate='sv update'
alias svstatusonserver='sv status --show-updates' # Show status here and on the server
alias svcommit='sv commit'
alias svadd='svn add'
alias svaddall='svn status | grep "^\?" | awk "{print \$2}" | xargs svn add'
alias svdelete='sv delete'
alias svhelp='svn help' 
alias svblame='sv blame'

# Aliases for starting stopping and restarting apache
alias apache_start='apachectl start'
alias apache_restart='apachectl stop'
alias apache_stop='apachectl restart'

# Alias for viewing hint when using mysql and forgetting to start mysql server
mysql(){
  echo "You can start the mysql server with mysql.server start" && command mysql "$@"
}


# Aliases for aalto-university
alias kosh='ssh jausoini@kosh.aalto.fi'
alias koshx='ssh -X jausoini@kosh.aalto.fi'
alias kekkonen='ssh jausoini@kekkonen.cs.hut.fi'
alias kekkonenx='ssh jausoini@kekkonen.cs.hut.fi'

# Aliases for ssh kapsi
alias kapsi='ssh ouzoinio@kapsi.fi'
alias servu='ssh web_dev@78.47.78.243'

# Alias for running bundle exec in ruby projects
alias be='bundle exec'
